# Barnraiser-AroundMe_C

## About Barnraiser

You can read more about the background of this repository here: [About_Barnraiser](About_Barnraiser.md)

The following is the original description of *AROUNDMe*, taken from its homepage http://barnraiser.org/aroundme

---
## AROUNDMe
AROUNDMe collaboration server is the perfect solution for anyone wishing to create collaborative social spaces on the Web.

If you want to extend your brand or build a business model around people forming groups on the web (think "Google groups", "Yahoo groups" or "Ning") then AROUNDMe collaboration server is the solution for you.

Using AROUNDMe collaboration server you can create multiple collaborative group, webspace, community or social networking websites. Each group can create a multiple page collaborative web site. They get social tools such as a guestbook (a wall), a group blog, a forum and a wiki which they can drop into web pages. Each group is fully customisable using xHTML, CSS, Javascript and PHP. Groups can be private or public.

People can connect to a group to contribute using OpenID. They get tracking tools such as forum digest and blog RSS feeds.

## Technical considerations
AROUNDMe requires a web server running either Apache 1.3/2.x or IIS5/IIS6 with PHP5.x installed including Curl, BCMath and GD library with MySQL 4.1 or MySQL 5.0 database.

AROUNDMe uses a domain name / sub domain as your group. You need to own or have access to a domain name to install this product.

---
This repo is based on the latest version aroundme_c_20080327.tar.gz found at http://barnraiser.org/aroundme. Older versions as well as the accompanying identity servers AroundMe_I and AroundMe_PI can be found at http://download.savannah.gnu.org/releases/aroundme/. I have also set up git-repositories for these two here.